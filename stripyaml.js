#!/usr/bin/env node
var fs = require("fs");

var file;

if (process.argv.length > 2) {
  file = fs.createReadStream(process.argv[2]);
}
else {
  file = process.stdin;
  file.resume();
}

file.setEncoding('utf8');
file.on('data', function(data) {
  // first, test where is the first empty line
  // Add +1 to compensate for EOL character
  var empty_line = data.search(/^\s*$/m);
//  console.log('empty_line = ' + empty_line);
//  console.log('data.length = ' + data.length);

  if ((empty_line != -1) && (empty_line != data.length)) {
    var pos = data.match(/^-+[\r\n]+/m);
    if (pos) {
      var divider_pos = pos.index + pos[0].length;
//      console.log('divider_pos = ' + divider_pos);
      if ((divider_pos - empty_line) < 3) {
//        console.log('pos.index = ' + pos.index);
//        console.log('divider_pos = ' + divider_pos);
        console.log(data.slice(divider_pos));
        return;
      }
    }
  }
  console.log(data);
});
